package model.common;

/**
 * 
 * Representing a point position in the gamespace.
 */
public interface Position {
    /**
     * 
     * @return the x coordinate of the position.
     */
    double getX();

    /**
     * 
     * @return the y coordinate of the position.
     */

    double getY();

    /**
     * Move actual position of a given movement.
     * 
     * @param movement the movement to setup.
     */
    void update(Movement movement);

    /**
     * @param x x offset
     * @param y y offset
     * @return a new position translate of an offset
     */
    Position getSumPosition(double x, double y);

}
