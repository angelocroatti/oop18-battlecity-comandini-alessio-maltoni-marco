package model.event.world;

import model.entities.Block;

/**
 * 
 * Represent an event that destroy a block in the world.
 */
public class BlockDestroyEvent extends WorldEvent {

    private final Block block;

    /**
     * 
     * @param block represent the block to destroy.
     */
    public BlockDestroyEvent(final Block block) {
        this.block = block;
    }

    /**
     * 
     * @return the block to be removed
     */
    public Block getBlock() {
        return block;
    }

}
