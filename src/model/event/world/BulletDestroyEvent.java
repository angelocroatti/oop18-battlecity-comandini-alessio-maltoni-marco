package model.event.world;

import model.entities.Bullet;

/**
 * Represent the event that destroy a bullet.
 *
 */
public class BulletDestroyEvent extends WorldEvent {

    private final Bullet bullet;

    /**
     * 
     * @param bullet the bullet to be destroy
     */
    public BulletDestroyEvent(final Bullet bullet) {
        this.bullet = bullet;
    }

    /**
     * 
     * @return the bullet to be destroy
     */
    public final Bullet getBullet() {
        return bullet;
    }

}
