package model.event.session;

import java.util.Optional;

import model.enemy.Enemy;
import model.entities.Tank;

/**
 * Event that represent the destroy of an enemy. This is to handle to add point
 * to the player.
 */
public final class EnemyDestroyedEvent implements SessionEvent {

    private final Tank killerTank;
    private final Optional<Enemy> destroyedType;

    /**
     * Default constructor.
     * 
     * @param killerTank    the tank who kill the enemy
     * @param destroyedType the type of tank destroyed
     */
    public EnemyDestroyedEvent(final Tank killerTank, final Optional<Enemy> destroyedType) {
        this.killerTank = killerTank;
        this.destroyedType = destroyedType;
    }

    /**
     * @return the type of the destroyed enemy.
     */
    public Optional<Enemy> getDestroyedType() {
        return destroyedType;
    }

    /**
     * Method that allow o know the source of the bullet to find it in the player
     * list The tank must be of the player.
     * 
     * @return the tank who killed that enemy.
     *
     */
    public Tank getKillerTank() {
        return killerTank;
    }

}
