package model.entities;

import enums.Sprite;
import model.common.MovementImpl;
import model.common.Position;

/**
 * Implementation of {@link Block}.
 *
 */
public final class BlockImpl extends AbstractGameEntity implements Block {

    private final Type type;

    /**
     * Constructor that set a static movement block.
     * 
     * @param sprite   the sprite of the block.
     * @param position the position of the block.
     * @param type     the type of the block.
     */
    public BlockImpl(final Sprite sprite, final Position position, final Type type) {
        super(sprite, position, new MovementImpl(), type.getDimension());
        this.type = type;

    }

    @Override
    public Type getType() {
        return this.type;
    }

}
