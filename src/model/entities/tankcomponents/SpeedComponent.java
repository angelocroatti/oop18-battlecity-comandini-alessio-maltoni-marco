package model.entities.tankcomponents;

import model.entities.Tank;

/**
 * 
 * A component that modify the speed of the tank to a given delta.
 */
public final class SpeedComponent implements ActiveTankComponent {

    private static final double DEFAULT_SPEED_BOOSTER = 2;
    private final Tank attachedTank;
    private final double speed;

    /**
     * Constructor that multiply x2 the speed of the attachedTank.
     * 
     * @param attachedTank tank to make
     */
    public SpeedComponent(final Tank attachedTank) {
        this(attachedTank, DEFAULT_SPEED_BOOSTER);
    }

    /**
     * Constructor that allow multiply the speed to a given delta.
     * 
     * @param attachedTank the current attached tank.
     * @param deltaSpeed   value 1.0 maintain current speed, 2.0 double the speed,
     *                     0.5 halved the speed, all other double value are allowed.
     */
    public SpeedComponent(final Tank attachedTank, final double deltaSpeed) {
        super();
        this.attachedTank = attachedTank;
        this.speed = deltaSpeed;
    }

    @Override
    public void useComponent() {
        this.attachedTank.getActualMovement().mul(speed);
    }

}
