package model.entities;

/**
 * 
 * An interface that represent a bullet in the world.
 */
public interface Bullet extends GameEntity {

    /**
     * 
     * @return the power of the bullet
     */
    Power getPower();

    /**
     * Represent the bullet power, if its normal it can destroy wall block, if is
     * armor piercing it can destroy iron block.
     *
     */
    enum Power {
        NORMAL, ARMOR_PIERCING
    }

    /**
     * This method is needed for the mechanical of the collision with other tank.
     * 
     * @return the tank that shoot the bullet.
     */
    Tank getAttachedTank();

}
