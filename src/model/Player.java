package model;

import java.util.Map;

import model.common.Counter;
import model.enemy.Enemy;
import model.entities.Tank;

/**
 * Representing a player in the game. A player have some life, a default initial
 * position, a map of current killed tank to manage points.
 *
 */
public interface Player {
    /**
     * Get the tank of the player.
     * 
     * @return the tank attached to the player;
     */
    Tank getTank();

    /**
     * Get the points of the player calculated by sum the killed tank.
     * 
     * @return the total points of the player.
     */

    int getPoints();

    /**
     * Get the life of the player that manage the number of kills that a player can
     * handle.
     * 
     * @return the current life of the player.
     */
    int getLife();

    /**
     * Reset the tank position of the player, to setup when a player is killed.
     */
    void initializeTankPosition();

    /**
     * Get the killed enemy type and how many time that player killed that type of
     * enemy.
     * 
     * @return the map of with the enemy current killed and his counter
     */
    Map<Enemy, Counter> getKilledTank();

    /**
     * Decrement the number of life of the player.
     */
    void decrementLife();

}
