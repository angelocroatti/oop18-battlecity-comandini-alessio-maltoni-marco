package model.command;

import model.entities.Tank;

/**
 * Abstract implementation of a movement command.
 *
 */
public class AbstractMovementCommand implements Command {

    private final Direction direction;

    /**
     * default constructor that setup the current direction.
     * 
     * @param direction the direction to setup
     */
    public AbstractMovementCommand(final Direction direction) {
        this.direction = direction;
    }

    @Override
    public final void execute(final Tank tank) {
        tank.getActualMovement().setMovement(direction.getXDirection(), direction.getYDirection());
        tank.setDirection(direction);
    }

}
